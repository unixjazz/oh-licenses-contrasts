# Open Hardware Licensing: parallels and contrasts

This repository includes the base files for the article "Open Hardware Licensing: parallels and contrasts" prepared for the European Comission in August, 2019. This case study is part of Open Science Monitor led by the Lisbon Council together with CWTS and ESADE.

## Files

1. Article text in TXT, ODT, and PDF formats
2. Images in SVG and PNG formats

## Authors

Luis Felipe R. Murillo – School of Data Science, University of Virginia

Pietari Kauttu	– CERN Knowledge Transfer, CERN	

Laia Pujol Priego – Ramon Llull University, ESADE

Andrew Katz – Moorcrofts LLP, University of Skövde, Sweden

Jonathan Wareham – Ramon Llull University, ESADE

## License

All files in this repository are licensed as CC-BY-4.0 International
